<div align="right" >
<p align="right" >
<img src="https://raw.githubusercontent.com/OpeningDesign/OD_Marketing/master/Logos/od_icon_logo_2.jpg" width="120px"/>
</p>
</div>

<!--
<div align="right" >
<p align="right" >
<img src="https://dl.dropbox.com/s/64fehzmn6sfaq1g/PHD%20logo.png?dl=1" width="300px"/>
</p>
</div>
-->

### Proposal for [OpeningDesign's](http://openingdesign.com/) Architecture and Engineering<!-- & Structural Engineering& Interior Design Engineering--> Services

<!--



### Proposal for the following services<!--[OpeningDesign's](http://openingdesign.com/) 
- Interior Design
- Furniture, Fixtures, and Equipment (FF&E) Selection and Procurement
- Architecture and Engineering

-->


---

**FOR:**

> 

---


**Project Location**
> 


Hi Michele/Justin, 

Was a pleasure chatting the other day. 

We're excited to have the opportunity to share the following proposal.

Although a more nuanced list of requirements will undoubtedly unfold as the project evolves, on a high-level, I understand that this project will include the following list of requirements/priorities.


- Rework kitchen/dining area
  - make more open to the living room
  - Remove exterior door at utility room
- Potentially expand the master bedroom to make more spacious and efficient
- Explore the option of expanding the mezzanine above the living room--potentially to include offices
- Potentially include new grand stairwell that ascends 3 floors
- Larger bathroom for near the upstairs bedrooms
- Built-in desk for east bedroom
- Include 2nd living room in the basement
  - with potential immediate access to ground floor patio area
- Potentially include a utility/work room in basement, with immediate access to the outside. 
  - could be used for sewing, as well. 
- Look at ways to potentially reroute ductwork in basement to increase head height

<!--

>The above list is also included in the following online document... 
https://docs.google.com/document/d/125-xQcU_22aZwW-1699Hmp7ui3n38U7tFa5BBfvOMRc/edit
>
>Feel free, at any time, to add/modify this document if you have additional thoughts.  This will be a living document throughout the duration of the design. 
-->



The following is a breakdown of services and phases we anticipate for your project.

---


## Phases & Scope of Services

### **Phase 1 - Programming** <!--*(Fees associated with this phase are discounted at 25%)*-->
  
  <!--* Architecture-->
  
  * Conduct zoning & building code analysis
  * Measure, model, and draft up the existing conditions relative to the scope of work
    <!-- * If there's existing as-built drawings of the space, less effort would be necessary for this phase.  Even less so, if there are CAD-based drawings.  Although hardcopy (paper) drawings will also be helpful.  Either way, however, we will still need to conduct site measurements to verify dimensions. -->
  * The city might require an official exterior site survey of the existing area.  <!-- If needed for any plan commission meetings, or other site approvals by the city. -->  If required, we assume this existing site survey would be provided by a 3rd party surveyor, along with a CAD background.
    <!-- * This proposal does not include a site survey-which we will need for this project.  We assume a survey of the existing site (CAD format) will by supplied by a 3rd party surveyor. Ideally, the survey would include: -->
    <!-- * locations of all structures on property -->
    <!-- * location of impervious areas -->
    <!-- * property lines with metes and bounds -->
    <!-- * recorded easements -->
    <!-- * utility locations (buried and overhead) -->
      <!-- * inside the site -->
      <!-- * and in the adjacent right of way (streets) -->
    <!-- * topographical elevations -->
    <!-- * locations of large trees (around new building location only - only if preserving the trees are required) -->
    <!-- * locations of existing houses/structures on adjacent properties (used to determine allowed lakeshore setbacks) -->
      <!-- * This proposal does not include a site survey-which we will need for this project.  We assume a survey of the existing site (CAD format) will by supplied by a 3rd party surveyor. -->
  * At the end of this phase, we provide up to (2) diagrammatic floor plan options which will explore ideal layout and adjacencies of the various spaces throughout the project, as well, as their relation to the site.  These design options will address varying approaches in the following criteria.
    * Functional relationship of the various spaces
    * Code restrictions/requirements
    * Structural considerations
    * Mechanical considerations
    * Major equipment locations
    * Plumbing considerations
    * Environmental factors
    * Optimizing Daylight
    * Acoustical considerations
    * Existing site layout
    * Furniture/equipment requirements
  * A few examples of these diagrammatic floor plans
    * [Example 1](https://www.dropbox.com/s/bblk8t8dmqsiflz/20211104%20-%20101_W_33rd_St%20-%20Site%20Layout%20Options.pdf?dl=1)
    * [Example 2](https://www.dropbox.com/s/aehy9hh5f2ih75b/20211118%20-%20Kegonsa_East%20-%20Layout%20Options%20and%20Existing%20Floor%20Plans.pdf?dl=1)
   


<!--

* Civil Engineering *(see attached proposal for details)*
        * Boundary Survey and Title Analysis
        * Topographic Collection
        * Existing Site Plan
        * Soils Analysis (Infiltration) 

    * This is the phase where we help you define and refine the problem--that is, we help you tease out the list of problems you would like to solve with this renovation.  The online, real-time xxx, mentioned above, will be the location where both parties can log your list of requirements and prioritizes.


Although not crucial,  in these earlier phases it's best practice to supply an official survey of the site, as early as possible as it assures the proposed design, as it is laid out on the site, is based on accurate information.  It eliminates the potential for any surprises and rework further down the line.

-----------------------------------------------------------------------------------------------------------------------
    * Civil
        * Assemble and compose site survey
            * Elevation contours
            * Property Lines
            * Document existing site features
            * Document setbacks/easements
            * Locate existing utilities 
                * Water
                * Sewer
                * Electrical

-----------------------------------------------------------------------------------------------------------------------
        * Conduct research on list of spaces/rooms 

-----------------------------------------------------------------------------------------------------------------------

        * Conduct structural assessment    
    * We can provide a survey of the property, but this proposal assumes one will be provided
    * Provide quick, single-line, site studies and floor plan diagrams 


-----------------------------------------------------------------------------------------------------------------------

    * LEED
        * Facilitate workshop with owner and the various consultants/contractors to determine [LEED credits](https://github.com/OpeningDesign/OD_Library/blob/master/Green/LEED%20Credits%20-%20New%20Construction%20-%20LEED%20v4.md) to prioritize 
        * Register project with the U.S. Green Building Council’s (USGBC’s) Leadership in Energy and Environmental Design (LEED) certification program

-->



### **Phase 2 - Schematic Design**
  
* During this phase we will start developing what the look and feel of the project will be.  We will provide up to (2) design options illustrated with the following documentation: 
 * Architecture & Interior Design
     <!-- * Site Plan -->
     * Floor Plans
     * Interior Elevations, if necessary *(key areas only)*
     * Building Sections, if necessary
     * Elevations<!-- , *(Of the scope area only, not the entire building)* -->
	  * 3D renderings/animations
        * [Here's](https://www.youtube.com/playlist?list=PL35WBXQO6nuCUDa5jHtURHCcOp3jXOrtc) a couple examples of past animations we've done.<!-- , as well as renderings, [here](http://openingdesign.com/rendering_examples/). -->
    <!-- * Start coordinating and eliciting feedback from the various subs and/or engineering disciplines. -->
    * Will begin to suggest a palette of exterior/interior materials (wall, flooring, ceiling, and millwork, etc.)
      <!--* Precedent studies and mood boards
        * Furniture, Fixtures, and Equipment (FF&E)
    <!-- - Floor plans will take initial stabs at suggested furniture layout -->


<!--

 -----------------------------------------------------------------------------------------------------------------------

       * Provide a schematic/preliminary estimate for cost of construction or help GC with quantity takes-offs

-----------------------------------------------------------------------------------------------------------------------

    * LEED
        * Conduct energy modeling analysis to determine ideal orientation, shading strategies and glazing percentages to maximum energy efficiency
        * Compose and distribute **LEED Certification Plan** to consultants/contractors.  Includes.
            * LEED points targets
            * Suggested strategies for implementing
            * Roles and responsibilities of the various parties
            * Schedule
            * List of system/component to be certified
            * List of documentation required for submissions

-----------------------------------------------------------------------------------------------------------------------

-->

### **Phase 3 - Design Development**

   * Here we<!-- , along with our consultants,  --> will dial into one design by either refining one of the proposed designs and/or combining the desired aspects of the other design(s) proposed.
      * Architecture & Interior Design
          <!-- * Site Plan *(of the scope area only)* -->
          <!-- * We will base site plan on supplied 3rd party survey -->
          <!-- * Does not include, however, an official survey with metes and bounds. -->
          <!-- * Will coordinate with your civil engineer -->
          <!-- * We will use Google Aerial, along with our LIDAR scan, to get a general site layout, but this is not intended to be an official CSM (Certified Survey Map).  -->
          <!-- * For this project, we assume the AHJ will not require one. -->
          <!-- * For this project, depending on the exterior scope, the AHJ might require a CSM. -->
          <!-- * Will include landscaping plan for city review -->
          <!-- * Site Plan, if necessary for possible zoning or plan commission review -->
           * Floor Plans
           * Demolition plans<!-- , if applicable -->
          * Exterior Elevations (Of the scope area only, not the entire building)
          <!-- * Exterior elevation done for signage location only -->
           * Interior Elevations - *key areas only*
           * Building Sections
           * 3D renderings and animations, as described in Phase 2
           * Suggest and continue to refine the palette of finish materials. (wall, flooring, ceiling, and millwork, etc.)
          <!-- *  Will provide material boards with physical samples for review -->
           <!-- * Continue to coordinate and eliciting feedback from the various subs and/or engineering disciplines -->
        <!-- - Furniture, Fixtures, and Equipment (FF&E)  -->
        <!-- - Will continue to refine indoor furniture section -->
        <!-- - Will start coordinating with furniture vendors-->
    	   <!-- * At the end of this phase we should have sufficient documentation to achieve any type of local zoning and/or plan commission approvals that may be necessary. *Please note, this does not include the building permit, which will be applied for in later stages.*  -->
          <!-- * Please note, if these AHJ meetings are necessary, this proposal assumes one attempt at applying for a city approval. If a redesign is required, and/or additional applications are necessary, we can either charge hourly for our services, or you can request we provide an adjusted proposal before moving forward. -->
        <!-- * For this project, however, we do not anticipate any zoning and/or plan commission approvals. -->

   
       <!--* Civil Engineering (see attached proposal for details)
            * Grading and Erosion Control Plan
            * Shoreland Zoning Permit Application
            * Shoreland Erosion Control & Mitigation Report 
            * Vegetative Buffer Plan (If required)-->

---

> #### Early General Contractor(GC) Involvement
> 
> At the conclusion of this phase, we strongly advise sharing these drawings with your chosen contractor for preliminary estimates. Despite their 'schematic' nature at this point, the drawings will be developed sufficiently for your contractor to provide a rough estimate. While this estimate may have a margin of error due to the drawings not being 100% complete, an experienced general contractor should be able to provide an accurate enough assessment. This evaluation helps determine the financial feasibility of the planned scope and whether it's prudent to proceed to the more time-intensive fourth phase—Construction Documents.
>
>Moreover, involving an experienced General Contractor (GC) and potentially their subcontractors early in the process can yield valuable insights into local supply chains and construction conventions. This early involvement aids in value engineering the project, facilitating the achievement of target costs at an early stage. In essence, making design adjustments earlier in the process is more straightforward compared to value engineering a fully detailed project in the later stages of design.

<!-- >Having been in the business for many years, we would be happy to suggest a list of contractor we trust and enjoy working with.  Just let us know. --> 

<!--
        *  If necessary, we will help suggest qualified GCs to help with the estimates.

         * Civil Engineer
         * Structural Engineer
         * Mechanical Engineer
         * Electrical Engineer
         * Plumbing Engineer
         * Lighting Designer
         * Kitchen consultant


        * Civil 
            * *Some scope items might not be necessary depending on the municipality requirements*
               * Storm water management/calculations
               * Grading and erosion control plan/details
                * Site stabilization details/methods.
                * Sediment control measures
                <!--* Fire lane plan
                * Utility Plans
                    * Water
                    * Sewer
                    * Storm
                    * Fire Hydrant

-----------------------------------------------------------------------------------------------------------------------
    * LEED
        * Modify and update **LEED Certification Plan** as appropriate
        * Provide shortlist of potential green building products and materials

-----------------------------------------------------------------------------------------------------------------------

    * At the end of this phase we shall have sufficient documentation to apply for a zoning variance.



-->


### **Phase 4 - Construction Documents**

   * Out of all the phases listed in this proposal, the **Construction Document** phase is the most labor intensive.  This is the phase where we dial into the exacting details of the design.  We<!-- , along with our consultants, --> propose to provide the following construction documents for your project.
  
      * Architecture & Interior Design
          * Code summary
            <!-- * ADA requirements, if applicable -->
            <!-- * Life safety plan -->
            <!-- * Emergency and exit sign layouts -->
            <!-- * Site Plan, only of the scope area, not the entire site. -->
            <!-- * w/ topography, if contours/elevations were supplied by surveyor -->
            <!-- * This assumes the city does not need an official survey for the small amount of worked planned outside -->
            <!-- * Will be based on information supplied by 3rd party survey  -->
          * Demolition plans
          * Floor plans
          * Roof Plan
          * Reflected ceiling plan
          * Lighting
            * Will suggest lighting layout locations
            * Will help pick out decorative fixtures, if necessary
            * We assume the you and/or the electrician will spec the utility lighting
            * Does not include switching/lighting controls
            <!-- * We assume all new lighting will match existing -->
            <!-- * Does not include picking out fixtures -->
          * Finish Plans
          * Building Sections
          * Building Elevations
          * Wall Sections<!-- , if necessary  -->
          * Stair sections<!-- , if necessary -->
          * [Large scale](http://openingdetail.com/gallery/) exterior/interior details, as necessary
          * Interior elevations of key areas only
            <!-- * Includes millwork/casework elevations w/ proposed finishes -->
            <!-- * but does not include [large scale](http://openingdetail.com/gallery/) detailing of these components as we assume we will refine the details in coordination with the the casework contractor -->
            <!-- We assume all casework will match existing.  -->
            <!-- * Does not include millwork/casework elevations. We assume these will be design/built directly with the GC's or owner's vender. -->
            <!-- * Interior elevations are not included, but will include elevations and enlarged plans of ADA restroom layouts.  -->
          * Schedules
              * Simple Door Schedule<!--  and door hardware schedule -->
                  * does not include hardware, we will work with the GC's hardware vendor to dial in on specifics.
              * Window schedule<!-- , if necessary -->
              * Finish schedule
              <!--* We assume as majority of finishes will match existing-->
          * Energy code check, if necessary (COMcheck/REScheck for example)
          <!-- * General specifications -->
        
  
      * Structural
        * Foundation Plan<!-- , if necessary -->
          * > We recommend getting a soils report from a third-party geotechnical engineer to determine the exact bearing capacity of the soil, ensuring accurate foundation sizing. Without this report, we'll assume a soil bearing capacity of 2,000 psi, which may result in oversized (more expensive) foundations. If no report is provided, we are not responsible for soil bearing less than 2,000 psi.
        * Floor plans with structural member sizes
          * Does not include sizing of pre-engineered floor/roof trusses. These are usually sized by the building supplier through the GC.
        * Structural calculations<!-- , if necessary -->

      <!-- * We assume, however, as is typical with metal buildings, that the metal building manufacturer will submit all the necessary permit drawings and calculations for all the structural steel in the building. -->



<!--
    * Mechanical
        * Heating/cooling load calculations
        * Floor plans locating and sizing of
            * Equipment
            * Ductwork
            * Natural gas
        * Air terminal & damper schedules
        * Kitchen equipment hookup
        * Typical details
        * Energy code check
        * Specifications
        * Services not included:
            * Extensive energy modeling beyond prescriptive requirements
     * Electrical
        * Major Equipment Locations
        * Receptacle locations
        * Kitchen equipment hookup
        * Specifications
        * Services not included:
            * Low voltage design
            * Telecom design
            * audio/visual design
            * Security system
            * Fire alarm
            * Process equipment
      * Lighting
        * Circuiting
        * Layout
        * Fixture Selection
        * Site utilities
        * Site Lighting
        * Services not included:
            * Photometric Calculations
            * Lighting controls
            * Daylight modeling
      * Plumbing
        * Floor plans locating and sizing of
            * Equipment
            * Sanitary drainage and venting
            * Water distribution (hot/cold)
        * Plumbing risers
        * Kitchen rough-in sizes    
        * Site utilities 
        * Specifications
        * Services not included:
            * Fire protection (sprinkler) design
            * Process equipment hookup 
-->
   * At the end of this phase we should have sufficient documentation to submit for permit.  Often times, however, due to accelerated construction schedules, we will submit for permit a portion of the way through this phase, and complete the remaining documentation after the permit has been issued.
<!--

        * Structural
             * Specifications
             * Our assumption is that we will work closely with your contracted, or in-house structural engineer.  With their input and guidance, however, we will provide all the necessary drafting
    
    
        * Furniture, Fixtures, and Equipment (FF&E)
            * Finalize furniture selection, specification, and layout
            * Work with the various furniture vendors to finalize order and delivery schedule
            * Selection, specification and procurement of artwork, décor items, and art framing.-->



---


 
> #### Level of Detail
>Please recognize that the more comprehensive the construction documents, the 1) more precise the General Contractor's (GC) estimates will be, and 2) the fewer surprises will arise regarding the final construction. However, it's worth noting that detailed drawings come with higher design fees and an extended development timeline.
>
>In many of our construction projects, clients often find a balance. They aim to invest just enough in design fees to secure an accurate bid, understanding that some details may be refined in collaboration with the contractor(s) and OpeningDesign during the project. This approach is particularly applicable when there's a high level of trust with the GC from the early stages and is the suggested approach as mentioned above in the **Early General Contractor(GC) Involvement**  section, above.
>
>If, on the other hand, you prefer more thorough documentation, please inform us. We are open to adjusting this proposal accordingly.
>


<!--   


--------------------

        * Electrical/Low Voltage
            * Will provide electrical/low voltage layout, but does not include electrical engineering such as:
            * Branch circuit layouts & panel scheduling
            * Control panel/room layout & sizing
            * Electrical diagrams



-----------------------------------------------------------------------------------------------------------------------

            * Landscape plan
                * Proposed plantings and ground treatment

-----------------------------------------------------------------------------------------------------------------------

            *  Kitchen Equipment Hookup
            * Medical Piping

-----------------------------------------------------------------------------------------------------------------------

         * Commercial Kitchen
            * Floor Plans
            * Equipment List

-----------------------------------------------------------------------------------------------------------------------

      * Low Voltage Plan
        * Fire Alarm
        * Security Systems (if necessary)
        * Audio/Visual
        * Fire Protection
            * Sprinkler system (performance specifications only)
                * detailed fire suppression system is assumed to be provided by FP contractor 
            *  Sprinkler System (Detailed) * Hydraulic Calculations -    

-----------------------------------------------------------------------------------------------------------------------


        * LEED
            * Finalize green building products and materials
            * Develop 'green' construction specifications
            * Provide appropriate submittals and calculations for USGBC's *design phase* application
        * All disciplines
            * Provide construction specifications

-----------------------------------------------------------------------------------------------------------------------


-->

### **Phase 5 - Bidding and Issuing for Permit**



  * In the event you have yet to contract with a general contractor *(which we don't recommend waiting this long to engage with a GC for reasons noted above)*, we will facilitate a more formal bidding process in this phase.  That is, if deemed necessary to get additional bids, we will help distribute the final construction documents to your list of preferred general contractors. We are also happy to help you shortlist a number of qualified contractors as well.
  * Answer GC & subcontractor's bid questions, issue clarifications
  * Issue the drawings for building permit review
  * Provide the AHJ, reviewing the drawings, with any additional drawings and/or clarifications they may request after their initial review.  This happens quite often and is a normal part of the process.

> #### Post Permit Changes
> 
>At times, our clients may propose alterations relatively late in the process, typically in Phase 4 and beyond. If these changes are small enough and submitted early, we can usually incorporate them into the base bid. However, if they are substantial and submitted too late, we will inform you and issue a not-to-exceed change order for your approval before initiating any work.
>
>Should you choose to reissue a revised permit with these changes at a later date, please note that any additional work mandated by the Authority Having Jurisdiction (AHJ) for this revised permit is not covered in the base bid. Such additional work will be billed at an hourly rate as outlined in the enclosed billing rate schedule.
>
>While we will strive to make post-permit changes code compliant, if you opt not to resubmit the plans to the AHJ for reapproval, OpeningDesign cannot assume liability for potential rework resulting from drawings that are not code compliant upon re-review or inspection.
>
>Any change to the plans post-permit requires AHJ re-review and reapproval. It is your responsibility to inform us if you wish to resubmit. In some cases, major changes will necessitate resubmission to the AHJ, irrespective of their size.
>
>Given the multifaceted nature of the building code, which is sometimes subject to local interpretation, we require the approval of the governing body (AHJ) to mitigate both our and your risk of liability. This requirement applies not only to construction after the initial permit issuance but also to any work performed before the first permit issuance.

<!--

* Review and award bids

-->

### **Phase 6 - Construction Administration**

* Architecture & Interior Design
	* Site visits
		* Assuming (1) on average of every 4 weeks (additional site visits, if necessary, will be billed at hourly rates outlined in this proposal, including mileage costs)
	* Review/Respond to the following GC inquiries
    * RFIs (request for information)
    * Shop drawings and submittals
    * Material/Product substitutions proposed by the GC
    * Change orders
  * Develop punch list, if necessary


> #### Termination
> When construction starts, our job is not over.  Our involvement during this phase is typically just as important as the earlier phases, as quite often, due to accelerated schedules, we continue to work out the remaining details of the design.
> 
> In addition, our periodic on-site observation allows us to catch any construction errors and/or quality problems.
> 
>  Having said this, you are free to stop our services at any time, and at any phase, however, as our involvement is crucial in all the above phases, if you terminate our service before the end of construction, we would be required to remove our name as the architect/engineer of record from the AHJ (Authority Having Jurisdiction) and would no longer be professionally liable for any errors and omissions that might be exposed later in the project.  Our involvement throughout the project is crucial to minimizes our liability, as well as yours.
>
  > * Either party may terminate this agreement without the need to provide a specific cause. To initiate this termination, written email notice must be given to the other party. The agreement will officially conclude on the date stated in the notice.
    > *  In the event of a termination without a specific cause, it's important to note that the client is responsible for compensating OpeningDesign for services rendered and expenses incurred up to the termination date.
    > *  In case of termination, OpeningDesign or the client shall promptly return to the all documents, plans, or property owned by the other. 
    > *  In the event of termination without a specific cause, OpeningDesign shall make reasonable efforts to conclude any ongoing work or to facilitate a smooth transition to another architect. Please be aware that OpeningDesign will not assume responsibility for any delays, additional costs, or damages incurred by the client as a result of such termination.

<!--

---

    * Civil Engineering (see attached proposal for details)
    * House Offset Staking 
    * Boat House Offset Staking
    * As-Built Stormwater Management Certification
    * As-built House & Boat House Siting Certification (If required) 



       * Considering the site is quite far away from our home offices, site visits are not included in the proposal.  We will, however, conduct site visits upon request—billing rates and travel costs will apply.

    * LEED
        * Review and address any USGBC feedback from *design phase* application
        * Develop construction waste management plan
        * Assist GC with LEED tracking template
        * Provide appropriate submittals and calculations for USGBC's *construction phase* application
        * Review submittals for LEED compliance
        * Prepare a Final LEED Certification Report that documents
            * LEED rating achieved
            * Documentation submitted
            * USGBC reviews
            * LEED Certification Reviews

-->

---



## Services *not* included:

Although **we can provide** the following services, we assume either they are not necessary or will be provided by the GC, the 3rd party via the GC or directly contracted through you.

<!-- Although we can provide services in both Tiers, for those services in the **1st Tier**, we can provide the most value specific to your project. -->

*Please lets us know if you would like us to include any of the following.*

<!-- * ## 1st Tier -->
* As-built drawings for areas outside the scope.
  * Sometimes an AHJ will request additional floor plans outside the scope area
<!-- * Does not include any additional drawings (over what is outlined in the proposal above) that a Plan Commission might request.  -->
  <!-- * we assume no plan commission submission will be necessary for this project. -->
<!-- * Interior or Exterior Elevations -->
<!-- * Building/Walls Sections -->
<!-- * 3D Renderings/animations -->
<!-- * Interior design -->
  <!-- * Picking out interior finishes -->
  <!-- * Finish Schedule -->
<!-- * Window Schedule -->
<!-- * Site Plan -->
  <!-- * We will do a site plan of the scope area, but not the entire site plan. -->
<!-- * Structural Engineering -->
* Civil engineering, including, but not exhaustive:
    * Storm water management/calculations
    * Grading and erosion control plan/details
    * Site stabilization details/methods.
    * Sediment control measures
    * Retaining wall design
  <!-- * If the percentage of impervious surfaces on the site is proposed to go above Dane County's prescriptive requirements, we will need to consult with a civil engineer to development the following drawings/calculations -->
* Landscape design
* HVAC/Mechanical Engineering
  * We will, however, suggest routing of major ductwork
* Electrical Engineering
  <!-- * Lighting -->
    <!-- * Picking out and/or specifying light fixtures, we will, however help suggest fixture locations -->
    <!-- * Switching/lighting controls -->
    <!-- * Reflected ceiling plan -->
    <!-- * we can show outlet locations, however. -->
* Plumbing Engineering
  <!-- *[Large scale](http://openingdetail.com/gallery/) (3" or 6" = 1'-0") construction details -->
  <!-- * ## 2nd Tier -->
* Food Service or Commercial Kitchen Design
  * Kitchen equipment procurement
     <!-- * We will, however, suggest a schematic kitchen layout intended to be refined by a 3rd party kitchen vendor.  -->
     <!-- * For this schematic layout, we will require a list of equipment. -->
* Furniture, Fixtures & Equipment (FF&E) services
* High-piled combustible storage [documenation](https://up.codes/viewer/wisconsin/ifc-2015/chapter/32/high-piled-combustible-storage#3201.3) 
<!-- * Casework/millwork elevations -->
  <!-- * [Large scale](http://openingdetail.com/gallery/) casework/millwork details -->
<!-- * [Large scale](http://openingdetail.com/gallery/) interior details -->
* Long form (book) construction specifications
* Structural work associated with shoring during construction
* Land Surveying
* Low Voltage Design
* Audio/Visual Design
* Extensive energy modeling beyond prescriptive requirements
* LEED Design
* LEED Commissioning
* Hyper-Realistic Renderings
* Detailed Cost Estimation
* Fire Alarm
* Fire Protection (Sprinklers)
* Security
* Signage Design/Layout
* Acoustical Engineering Services
* Geotechnical Engineering
* Environmental Studies and Reports
* Materials testing or [special inspections](https://www.fandr.com/wp-content/uploads/2020/01/SiC-April-2017-Back-to-Basics.pdf)
* Information Technology
* Legal Survey
* Closeout Record Documents
* Testing and Balancing of Installed Equipment
* Environmental Studies
* Commissioning Services
* Moving Coordination
* Post-occupancy Elevation/Studies
* Maintenance and Operational Programming
* Building Maintenance Manuals
* Post-occupancy Evaluation
* Extensive decorative finish studies
* Art selection
* Artwork Production Services 
* Additional art commission services may be provided to client and quoted separately


<!--

----------------------------------------------------------------------------------

  * Energy code check (COMcheck for example)



----------------------------------------------------------------------------------
-->



## OpeningDesign's Aesthetic Leanings

We have found over the years, one of the hallmarks of a successful and unique project, is when the client and the architect are both vested and excited about the design.

In that regards, to give you a sense of what types of designs we are excited about, please review the following curated resources of architectural designs that have inspired us over the years.

* [Our Pinterest Board for Residential Architecture](https://www.pinterest.com/theoryshaw/-residential-architecture/)
<!-- * [Our Pinterest Board for Commercial Architecture](https://www.pinterest.com/theoryshaw/-architecture/) -->
  
If you are familiar with Pinterest, we would suggest going through these 2 boards and saving those images, to your own board, that resonate with you.  This will further help us dial into a look and feel that aligns with your taste, as well as ours.

Of course, our online [portfolio](http://openingdesign.com/portfolio/) and [gallery](http://openingdesign.com/gallery/) will also give you a sense of our aesthetic as well.



---



## Fees Relative to Desired Workflow

Over the years, OpeningDesign has developed a preferred and unique way of working.  Although OpeningDesign's <!--our--> website's [about page](http://openingdesign.com/about/) provides more detail, in a nutshell, most of our projects are [open source](http://en.wikipedia.org/wiki/Open_source) and are conducted [out-in-the-open](http://openingdesign.com/about/).

This open source concept might sound confusing, as it's a relatively new way of working in our industry, but this open way of working allows us to more agilely share work with our core group of independent architects and designers that we have worked with over the years.  In addition, this open platform always us to share, and build up a common library of details and drawings.

Simply put, by being able to bring in additional help quicker on project and sharing a common library of drawings, we are able to bring more value to our clients--both in cost and schedule.

We understand, however, some clients might prefer a more traditional approach to practicing architecture and interior design.  In that light, we offer (3) tiered fee options that vary from a fully open approach to a more private approach.

### Approach A

* **Approach (A): A Fully Open Approach** where the construction documentation, location, and the names of the parties involved in this contract are known and are shared publicly.  All of OpeningDesign (and their consultant's) newly created documentation/content is [open source](https://creativecommons.org/licenses/by-sa/4.0/).  That is, will be freely available to you, or any party, including the design team, for future use and possible redevelopment, assuming the terms such as [Attribution](https://creativecommons.org/licenses/by-sa/4.0/) and [ShareAlike](https://creativecommons.org/licenses/by-sa/4.0/) are honored. 
  
  * **Sample Projects**
	  * A multi-family residence in Eau Claire, WI.
		  * The CAD/BIM files can be found [here](https://github.com/OpeningDesign/CTR)
		  * [Here's](https://app.element.io/#/room/!qPmKPXfEHEqedAwbst:matrix.org) a  log of communication between the design teams, and our issue tracker [here](https://github.com/OpeningDesign/CTR/issues?q=).
	  * A **sport complex** in Jefferson, WI
		  * The CAD/BIM files can be found [here](https://github.com/OpeningDesign/Sports_Complex)
		  * [Here's](https://github.com/OpeningDesign/Sports_Complex/issues?q=is:issue+is:closed+sort:comments-desc) the log of communication between the design team and general contractor
	  * A **vacation rental** in Lake Geneva, WI.
		  * The CAD/BIM documents can be found [here](https://github.com/OpeningDesign/Vacation_Rental)
		  * [Here's](https://github.com/OpeningDesign/Vacation_Rental/issues?q=is%3Aissue+is%3Aclosed+sort%3Acomments-desc) a log of communication between the design team.
	  * A **residence** on Lake Kegonsa.
		  * The CAD/BIM documents can be found [here](https://github.com/OpeningDesign/Aalseth_Residence/tree/master/Models%20%26%20CAD/BIM)


### Approach B

* **Approach (B): An Anonymous Open Approach** Everything is the same as Approach A, however, the project location and the names of the direct or indirect clients, are kept anonymous and not shared publicly.
  * **Sample Project**
    * An **organic grocery store** in southern Wisconsin 
      * (CAD/BIM) files can be found [here](https://github.com/OpeningDesign/Organic_Grocery_Store).
    * An **office/warehouse** facility in southern Wisconsin
      * (CAD/BIM) files can be found [here](https://github.com/OpeningDesign/Open_Source_Metal_Building).
        
### Approach C

* **Approach &#40;C): The Traditional Approach** where all documentation, and clients involved, remains confidential and private.   Per industry norm, the designer and the designer’s consultants are deemed the authors and owners of their respective [Instruments of Service](https://corporate.findlaw.com/intellectual-property/llp-owner-vs-architect-who-owns-the-design.html), and they retain all common law and statutory rights, including copyright.
  * With this approach, clients are free to use these construction documentations during the construction, maintenance and any future additions/modifications of their one-off project. Unlike approach A and B, however, they are not free to use these plans to redevelop another project, on another site, without OpeningDesign's permission.  Again, this is more in line with the traditional way practicing architecture and interior design.
  * A vast majority of our clients choose Approach A or B, but we provide this approach for those clients who might not, for business reasons, share their plans publicly.  For example, a data center client might opt for this more confidential/private approach, as the design of their data center is a key asset to their business model. Another example, is a client that would like to keep their project confidential until they acquire the land and/or zoning approval, then switch to a more open approach later in the project.


#### Things kept private...

> * Please note, no matter which approach (A, B, or C) is used above...
>   * Any documentation from parties outside this contract, and/or shared with OpeningDesign prior and during the execution of this contract, **WILL NOT** be shared publicly.
>   <!--* Any documentation you share with OpeningDesign prior and during the execution of this contract, **WILL NOT** be shared publicly.-->
>   * Any prior emails, or any emails between you, OpeningDesign, or any other parties during the duration of the project, **WILL NOT** be shared publicly.
>   * Also, we will not make public anything that you explicitly indicate should be kept private.

---



> ##### *Although more common in other industries (software, for example), these open approaches are still relatively new in the construction industry and as such, if you have any questions please feel free to ask.  We would be happy to chat.  Ultimately we take great pride having teased out these open approaches over the years. It has proved a more efficient and cost effective way of practicing architecture--ultimately benefiting our clients and the overall industry in general.*
> 
> ##### Also, feel free to visit OpeningDesign's *[about](http://openingdesign.com/about/)* page.  It provides a little more information about these open approaches, as well.



## Not-to-Exceed Fee Proposal Options (**A**, **B**, or **C**):

####  The following table outlines the (3) fee options. <!--The fee percentages are based on the cost of construction for (1) development.-->

<table cellspacing="0" border="0">
	<colgroup width="75"></colgroup>
	<colgroup width="216"></colgroup>
	<colgroup width="10"></colgroup>
	<colgroup span="2" width="90"></colgroup>
	<colgroup width="10"></colgroup>
	<colgroup span="2" width="90"></colgroup>
	<colgroup width="10"></colgroup>
	<colgroup span="2" width="90"></colgroup>
	<colgroup width="10"></colgroup>
	<tbody><tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" rowspan="2" height="60" align="center" valign="middle"><b><font face="Century Gothic">%</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" rowspan="2" align="center" valign="middle"><b><font face="Century Gothic">Phase</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" colspan="2" align="center" valign="middle" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic">Approach  (A)</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" colspan="2" align="center" valign="middle" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic">Approach  (B)</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" colspan="2" align="center" valign="middle" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic">Approach (C)</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Rough % of Const. Costs</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Estimated Duration</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Rough % of Const. Costs</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Estimated Duration</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Rough % of Const. Costs</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Estimated Duration</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.1" sdnum="1033;0;0.00%"><font face="Century Gothic">10.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 1: <br>Pre-Design &amp; Programming</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.006" sdnum="1033;0;0.0%"><font face="Century Gothic">0.6%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="2.25" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">2.3 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0072" sdnum="1033;0;0.0%"><font face="Century Gothic">0.7%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="2.925" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">2.9 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0084" sdnum="1033;0;0.0%"><font face="Century Gothic">0.8%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="3.8025" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">3.8 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.15" sdnum="1033;0;0.00%"><font face="Century Gothic">15.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 2: <br>Schematic Design</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.009" sdnum="1033;0;0.0%"><font face="Century Gothic">0.9%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="3.375" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">3.4 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0108" sdnum="1033;0;0.0%"><font face="Century Gothic">1.1%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="4.3875" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">4.4 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0126" sdnum="1033;0;0.0%"><font face="Century Gothic">1.3%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="5.70375" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">5.7 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.2" sdnum="1033;0;0.00%"><font face="Century Gothic">20.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 3: <br>Design Development</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.012" sdnum="1033;0;0.0%"><font face="Century Gothic">1.2%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="4.5" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">4.5 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0144" sdnum="1033;0;0.0%"><font face="Century Gothic">1.4%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="5.85" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">5.9 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0168" sdnum="1033;0;0.0%"><font face="Century Gothic">1.7%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="7.605" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">7.6 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.3" sdnum="1033;0;0.00%"><font face="Century Gothic">30.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 4: <br>Construction Documents</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.018" sdnum="1033;0;0.0%"><font face="Century Gothic">1.8%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="6.75" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">6.8 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0216" sdnum="1033;0;0.0%"><font face="Century Gothic">2.2%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="8.775" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">8.8 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0252" sdnum="1033;0;0.0%"><font face="Century Gothic">2.5%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="11.4075" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">11.4 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.05" sdnum="1033;0;0.00%"><font face="Century Gothic">5.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 5: <br>Bidding and Issuing for Permit</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.003" sdnum="1033;0;0.0%"><font face="Century Gothic">0.3%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="1.125" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">1.1 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0036" sdnum="1033;0;0.0%"><font face="Century Gothic">0.4%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="1.4625" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">1.5 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0042" sdnum="1033;0;0.0%"><font face="Century Gothic">0.4%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="1.90125" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">1.9 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.2" sdnum="1033;0;0.00%"><font face="Century Gothic">20.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 6: <br>Construction Administration</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.012" sdnum="1033;0;0.0%"><font face="Century Gothic">1.2%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">n/a</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0144" sdnum="1033;0;0.0%"><font face="Century Gothic">1.4%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">n/a</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="0.0168" sdnum="1033;0;0.0%"><font face="Century Gothic">1.7%</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">n/a</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic"><br></font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="62" align="center" valign="middle" bgcolor="#DDDDDD" sdval="1" sdnum="1033;0;0.00%"><b><font face="Century Gothic">100.00%</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic">All Phases</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD" sdval="0.06" sdnum="1033;0;0.0%"><b><font face="Century Gothic">6.0%</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdval="18" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic">18.0 wks </font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD" sdval="0.072" sdnum="1033;0;0.0%"><b><font face="Century Gothic">7.2%</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdval="23.4" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic">23.4 wks </font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD" sdval="0.084" sdnum="1033;0;0.0%"><b><font face="Century Gothic">8.4%</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdval="30.42" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic">30.4 wks </font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic"><br></font></b></td>
	</tr>
</tbody></table>


<br>
<br>
<br>
<br>
<br>
<br>

#### Fee & Duration Based on Hypothetical Costs Of Construction

<table cellspacing="0" border="0">
	<colgroup width="192"></colgroup>
	<colgroup width="10"></colgroup>
	<colgroup span="2" width="90"></colgroup>
	<colgroup width="10"></colgroup>
	<colgroup span="2" width="90"></colgroup>
	<colgroup width="10"></colgroup>
	<colgroup span="2" width="90"></colgroup>
	<colgroup width="10"></colgroup>
	<tbody><tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" rowspan="2" height="119" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic">Fee &amp; Duration Based on Hypothetical Costs Of Construction</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" colspan="2" align="center" valign="middle" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic">Approach  (A)</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" colspan="2" align="center" valign="middle" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic">Approach  (B)</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" colspan="2" align="center" valign="middle" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic">Approach (C)</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Possible Not-to-Exceed Fee</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Estimated Duration</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Possible Not-to-Exceed Fee</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Estimated Duration</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Possible Not-to-Exceed Fee</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Estimated Duration</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="26" align="center" valign="middle" sdval="210000" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic">$210,000</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="12600" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$12,600</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="12.6" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">12.6 wks</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="15120" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$15,120</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="16.38" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">16.4 wks</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="17640" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$17,640</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="21.294" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">21.3 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="26" align="center" valign="middle" sdval="300000" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic">$300,000</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="18000" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$18,000</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="18" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">18.0 wks</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="21600" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$21,600</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="23.4" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">23.4 wks</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="25200" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$25,200</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="30.42" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">30.4 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="26" align="center" valign="middle" sdval="390000" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic">$390,000</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="23400" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$23,400</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="23.4" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">23.4 wks</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="28080" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$28,080</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="30.42" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">30.4 wks</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="32760" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$32,760</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="39.546" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">39.5 wks</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic"><br></font></b></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="50" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic">Percent of Construction Costs</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" colspan="2" align="center" valign="middle" bgcolor="#DDDDDD" sdval="0.06" sdnum="1033;0;0.0%"><b><font face="Century Gothic">6.0%</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" colspan="2" align="center" valign="middle" bgcolor="#DDDDDD" sdval="0.072" sdnum="1033;0;0.0%"><b><font face="Century Gothic">7.2%</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><font face="Century Gothic"><br></font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" colspan="2" align="center" valign="middle" bgcolor="#DDDDDD" sdval="0.084" sdnum="1033;0;0.0%"><b><font face="Century Gothic">8.4%</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic"><br></font></b></td>
	</tr>
</tbody></table>



> # Please note, these are not **lump sum** fees, but are instead, **not-to-exceed** fees, which are based on a percentage of construction costs.  The **Hourly Rates**, called out below, will apply until this not-to-exceed fee is reached.  
> # The (3) fee and duration examples provided above, based on (3) hypothetical costs of construction, provide an example of how our fees are relative to the cost of construction.  That is, if the project costs go down/up, or less/more scope is involved over time, these fee ceilings will adjust relatively.
> 
> Please note, these not-to-exceed fees are based on the percentages of projected construction costs at any particular point in the project.  For example, if a project starts out with projected costs of 'X', but the scope is cut in half later in the project, the fees for that earlier phase will still be based on 'X'. 
> 
> We also assume your GC will share ongoing construction cost projections throughout all phases of the project.  <!--We also assume you will not act as your own GC.  -->
> 
> In the event there's a dispute over our fees or the project stops prematurely and/or changes scope before retaining a GC to determine projected construction costs, we require you to retain the services of a mutually agreed upon 3rd party estimator to determine a fair construction cost on which our not-to-exceed fee could be based.
> 
> Please note the not-to-exceed fee is applied to the entire project fee, and not the individual fees associated with each discipline and does do not include reimbursable expenses.
> 
> By using hourly rates and not-to-exceed fees, based on percentages of construction, we have found this to be a win-win for both parties.  The design professionals are given a little more safeguard against potential [scope creep](https://en.wikipedia.org/wiki/Scope_creep) and the client can realize more economical fees if they are able to make decisions quicker and more consistently--moving the design of the project along quicker.  In addition, clients are able to adjust, on the fly, what types of services they might or might not need as the project unfolds, thus avoiding tedious renegotiation.

---

<!--


## Product Markup Fee


-  To cover additional management services required to facilitate ordering, delivery, coordination and installation of the pieces, as well as addressing potential issues with the order, our fees include markup on furniture and accessories. All payments necessary to place orders are to be invoiced by Designer and to be collected in full prior to placement of the order. 
> Please Note: product markups, will not impose added cost charged to you, but rather are built-in into the retail cost of products through a discounted rate offered to us through our various vendor partners. Furthermore, with some vendors, we might be able to offer additional discounts to pass along to you -the exact discount amount, if available, will vary depending on the designer discount program offered by that particular vendor. If no trade pricing is available for the article of furniture or accessory, a 10% markup will be added to all orders procured by Designer.


-->




## Hourly Rates

<!--
> Please Note:  Services performed in Phase 1 (Programming) will be discounted **25%** discount.
-->

<table cellspacing="0" border="0">
	<colgroup width="133"></colgroup>
	<colgroup span="3" width="85"></colgroup>
	<tbody><tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="38" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic">Discipline</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic">Approach<br>(A)</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic">Approach<br>(B)</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic">Approach<br>(C)</font></b></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan="4" height="17" align="left" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic">Architecture</font></b></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="17" align="left" valign="middle"><font face="Century Gothic">Principal</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="120" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$120/HR</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="135" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$135/HR</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="150" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$150/HR</font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="17" align="left" valign="middle"><font face="Century Gothic">Project Architect</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="100" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$100/HR</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="115" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$115/HR</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="130" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$130/HR</font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="17" align="left" valign="middle"><font face="Century Gothic">Intern</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="75" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$75/HR</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="90" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$90/HR</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="105" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$105/HR</font></td>
	</tr>
</tbody></table>


<!-- > ### *The Consultants fees will include an **additional 15%** to cover in-house administration, handling, financing, and insurance costs.* -->



<!-- 

---

## The Determination of the Not-to-Exceed Fee

For your reference and peace of mind, please review the following 3rd party document(s) as to what the standard practices are for establishing design fees in the construction/architecture industry.

* [Architectural Fees (part 1)](http://www.lifeofanarchitect.com/architectural-fees-part-1/) by Bob Borson
 * [Architectural Fees…part two](http://www.lifeofanarchitect.com/architectural-fees-part-two/) by Bob Borson
 * [Architectural Fees for Residential Projects](http://www.lifeofanarchitect.com/architectural-fees-for-residential-projects/) by Bob Borson
* [A Guide to Determining Appropriate Fees  for the Services of an Architect](https://network.aia.org/HigherLogic/System/DownloadDocumentFile.ashx?DocumentFileKey=f7ea7369-a39e-4310-837d-f0d0d0c06270) from  Royal Architectural Institute of Canada

As you will see relative to the suggested fees outlined in these documents, our fees are competitive in comparison.  We are confident that through our unique and open way of working and our strong band of collaborators and consultants, that that we will meet and exceed the industry's standard of care. -->




<br>


---

## Reimbursable expenses include:

* Transportation in connection with the project for travel authorized by the client (transportation, lodging and meals)
  * $1.00 per mile for travel
  * Hourly billing rates while traveling
* Communication and shipping costs (long distance charges, courier, postage, dedicated web hosting, etc.)
* Reproduction costs for plans, sketches, drawings, graphic representations and other documents
* Renderings, models, prints of computer-generated drawings, mock-ups specifically    requested     by the client
* Certification and documentation costs for third party    certification    such    as LEED
* Fees, levies, duties or taxes for permits, licenses, or approvals from authorities having jurisdiction
* Additional insurance coverage or limits, including additional professional liability insurance requested by the client in a excess of that normally carried by the designer and the designer’s consultants
* Direct expenses from additional consultants not specifically outlined in this proposal

*Reimbursable expenses include an additional 15% to cover in-house administration,    handling,    and    financing.*

---

## Boilerplate

* We will deliver invoices on a monthly basis based on scope complete, with payment due within 30 days of receipt.  Invoices overdue past (60) days will be interpreted as an order to stop work on the project and a basis for termination of contract.
* If project's duration extends past 365 days, Hourly Rates, as outlined above, will increase by 10% each additional year that passes. 
* We are not responsible to select a general contractor or guarantee workmanship performed by contractor or other parties. We can, however, make suggestions or recommendations for craftsman or contractors for Client to consider
* We not responsible for the exploration, presence, handling, and/or adverse exposure of any hazardous materials, in any from. Including, but not limited to asbestos products, polychlorinated biphenyl (pcb) or other toxic substances.
* Both Parties agree to communicate about any pertinent information and known changes to the project scope in a timely manner by writing.
* In the event of premature project termination, we will bill for percentage of work completed to date, based on the enclosed hourly rate.
* If you decide to forgo the project, fees, from all phases complete, are due regardless of the following scenerios:
  * Construction costs are more than you expected, for whatever reason.
  * Code requirements, required by the AHJ, at any phase of the project (other than the 6th phase (Construction Administration), make construction costs too costly to continue with the project. 
  
  <!--* If no site survey is supplied, this proposal does include services to change the proposed design if the base assumptions such as property line locations and setbacks, as provided by our clients, is incorrect at later stages of the design.-->
* This proposal is valid for 90 days.

<!-- *LIMITATION OF LIABILITY
    * *In recognition of the relative risks and benefits of the Project to both the Client and the Consultant, the risks have been allocated such that the Client agrees, to the fullest extent permitted by law, to limit the liability of the Consultant and Consultant's officers, directors, partners, employees, shareholders, owners and subconsultants for any and all claims, losses, costs, damages of any nature whatsoever or claims expenses from any cause or causes, including attorneys' fees and costs and expert-witness fees and costs, so that the total aggregate liability of the Consultant and Consultants officer's, directors, partners, employees, shareholders, owners and subconsultants shall not exceed $_________, or the Consultant's total fee for services rendered on this Project, whichever is greater. It is intended that this limitation apply to any and all liability or cause of action however alleged or arising, unless otherwise prohibited by law.* -->

---

We sincerely appreciate the opportunity to submit this proposal and look forward to the potential of a fruitful collaboration in the future.

If I <!--we--> have included a service, within this proposal, that is not necessary and/or one that you would like to include, please let me <!--us--> know. 

If the terms of this proposal are acceptable please sign in the space offered below and remit a $4,000 retainer.

Finally, please don't hesitate to contact me should you have any questions or need clarification about the proposal--would be more than happy to sit down and have a more nuanced discussion.

<br>

Kind Regards,

<img src="https://dl.dropboxusercontent.com/s/wwm6lvp04kvj6cn/signature.png?dl=1" width="200px"/>

Ryan Schultz
ryan.schultz@openingdesign.com
773.425.6456
17 S. Fairchild, FL 7
Madison, Wisconsin 53703


<br>
<br>
<br>
<br>
<br>
<br>



### Authorized by:

* Please check the box, below, of your preferred Fee Proposal Option
  
  * ( ☐ A ) Fully Open Approach
  * ( ☐ B ) Anonymous Open Approach
  * ( ☐ C ) Traditional Approach



<!--
  * *Or, if you prefer, you can choose the fee option per phase.  Some clients, for example, might choose option **&#40;C)** until they buy the land and/or get zoning approval , after which, switch to option **(A)**.*
    
    * Phase 1 - Programming ( ☐ A ) ( ☐ B ) ( ☐ C )
    * Phase 2 - Schematic Design ( ☐ A ) ( ☐ B ) ( ☐ C )
    * Phase 3 - Design Development ( ☐ A ) ( ☐ B ) ( ☐ C )
    * Phase 4 - Construction Documents ( ☐ A ) ( ☐ B ) ( ☐ C )
    * Phase 5 - Bidding and Construction Contract Negotiation ( ☐ A ) ( ☐ B ) ( ☐ C )
    * Phase 6 - Construction Administration ( ☐ A ) ( ☐ B ) ( ☐ C ) 

-->



.................................................................................................................................................................................

* Signature:

.................................................................................................................................................................................



* Title:
  
.................................................................................................................................................................................



* Company:

.................................................................................................................................................................................


* Date:

.................................................................................................................................................................................



###  License

Per usual, unless otherwise noted, all content associated with [OpeningDesign](http://openingdesign.com) projects is licensed under an open source, 'copyleft' license: [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/) ![Attribution-ShareAlike 4.0 International](http://i.creativecommons.org/l/by-sa/3.0/88x31.png).  Attribution to be given to the entire team associated with the project.
