<div align="right" >
<p align="right" >
<img src="https://raw.githubusercontent.com/OpeningDesign/OD_Marketing/master/Logos/od_icon_logo_2.jpg" width="120px"/>
</p>
</div>

<!--
<div align="right" >
<p align="right" >
<img src="https://dl.dropbox.com/s/64fehzmn6sfaq1g/PHD%20logo.png?dl=1" width="300px"/>
</p>
</div>
-->

### Proposal for [OpeningDesign's](http://openingdesign.com/) Architecture & Structural Engineering<!--& Interior Design Engineering--> Services

<!--

### Proposal for the following services<!--[OpeningDesign's](http://openingdesign.com/) 
- Interior Design
- Furniture, Fixtures, and Equipment (FF&E) Selection and Procurement
- Architecture and Engineering

-->

---

For:
>

---



Hi Nike/Harry,

Thank you for the chat the other day. We’re excited to have the opportunity to share the following proposal.

<!-- Although a more nuanced list of requirements will undoubtedly unfold as the project evolves, on a high-level, I understand that this project will include the following list of requirements/priorities.

- New ~17'x ~24' garage off the back of the existing garage.
- (2) windows and an exterior door -->


The following is a breakdown of services and phases we anticipate for your project.

---



## Phases & Scope of Services

### **Phase 1 - Programming** <!--*(Fees associated with this phase are discounted at 25%)*-->
  
  <!--* Architecture-->
  
  * Conduct zoning & building code analysis
   * Measure, model, and draft up the existing conditions relative to the scope of work
	

### **Phase 2 - Schematic Design & Phase 3 - Design Development**

   * Architecture & Interior Design
       * Site Plan
         * We will use a Google aerial and GIS documentation<!-- , along with our LIDAR scan,  --> to get a general site layout, but this is not intended to be an official **Certified Survey Map**.
           * For this project, we assume the AHJ will not require one.
        * Floor Plans
          * This only includes the scope area, not the entire floor plan or existing building.
      <!-- * Interior Elevations - key areas only -->
      <!-- * Building Sections -->
        * Exterior Elevations
      		* Of the scope area only, not the entire building.
        <!-- * Interior Elevations - key areas only -->
        <!-- * Building Section -->
        <!-- * (1) Wall Section -->
   


### **Phase 4 - Construction Documents**
  
   * Architecture & Interior Design
     * In this phase we further refine the list of drawings mentioned above--in preperation of submitted to the AHJ for plan review <!--  We will also include the following drawings, as well.  -->
       <!--* Code summary
       * ADA requirements
       * Life safety plan
       * Emergency and exit sign layouts-->
       <!-- * Roof Plan -->
<!-- * Structural -->
  <!-- * Foundation plan -->
  <!-- * Floor plans with structural member sizes -->
  <!-- * Structural calculations, if necessary -->


### **Phase 5 - Bidding and Issuing for Permit**
  
    
   <!--* If necessary, a bidding set to a predetermined list of GCs
  * Answer GC & subcontractor's bid questions, issue clarifications-->
  * Issue the drawings for building permit review
    * Provide the city/town/village, reviewing the drawings, with any additional drawings and/or clarifications they may request after their initial review.  This happens quite often and is a normal part of the process.

> #### Post Permit Changes
> 
>At times, our clients may propose alterations relatively late in the process, typically in Phase 4 and beyond. If these changes are small enough and submitted early, we can usually incorporate them into the base bid. However, if they are substantial and submitted too late, we will inform you and issue a not-to-exceed change order for your approval before initiating any work.
>
>Should you choose to reissue a revised permit with these changes at a later date, please note that any additional work mandated by the Authority Having Jurisdiction (AHJ) for this revised permit is not covered in the base bid. Such additional work will be billed at an hourly rate as outlined in the enclosed billing rate schedule.
>
>While we will strive to make post-permit changes code compliant, if you opt not to resubmit the plans to the AHJ for reapproval, OpeningDesign cannot assume liability for potential rework resulting from drawings that are not code compliant upon re-review or inspection.
>
>Any change to the plans post-permit requires AHJ re-review and reapproval. It is your responsibility to inform us if you wish to resubmit. In some cases, major changes will necessitate resubmission to the AHJ, irrespective of their size.
>
>Given the multifaceted nature of the building code, which is sometimes subject to local interpretation, we require the approval of the governing body (AHJ) to mitigate both our, and your risk of liability. This requirement applies not only to construction after the initial permit issuance but also to any work performed before the first permit issuance.



### **Phase 6 - Construction Administration**

* Architecture & Interior Design
	* Site visits
		* Assuming 1 every 4 weeks (additional site visits, if necessary, will be billed at hourly rates outlined in this proposal, including mileage costs)
    * Respond to RFIs (request for information) from the GC
    * Review shop drawings and submittals, if necessary
      <!-- * Material/Product substitutions proposed by the GC -->
      <!-- * Change orders -->
      <!-- * Develop punch list, if necessary -->
       

<br>

> #### Termination
> When construction starts, our job is not over.  Our involvement during this phase is typically just as important as the earlier phases, as quite often, due to accelerated schedules, we continue to work out the remaining details of the design.
> 
> In addition, our periodic on-site observation allows us to catch any construction errors and/or quality problems.
> 
>  Having said this, you are free to stop our services at any time, and at any phase, however, as our involvement is crucial in all the above phases, if you terminate our service before the end of construction, we would be required to remove our name as the architect/engineer of record from the AHJ (Authority Having Jurisdiction), and would no longer be professionally liable for any errors and omissions that might be exposed later in the project.  Our involvement throughout the project is crucial to minimizes our liability, as well as yours.
>
  > * Either party may terminate this agreement without the need to provide a specific cause. To initiate this termination, written notice must be given to the other party. The agreement will officially conclude on the date stated in the notice.
    > *  In the event of a termination without a specific cause, it's important to note that the client is responsible for compensating OpeningDesign for services rendered and expenses incurred up to the termination date.
    > *  In case of termination, OpeningDesign or the client shall promptly return to the all documents, plans, or property owned by the other. 
    > *  In the event of termination without a specific cause, OpeningDesign shall make reasonable efforts to conclude any ongoing work or to facilitate a smooth transition to another architect. Please be aware that OpeningDesign will not assume responsibility for any delays, additional costs, or damages incurred by the client as a result of such termination.



---
<br>
<br>
<br>
<br>

## Services *not* included:

Although **we can provide** the following services, we assume either they are not necessary or will be provided by a 3rd party via the GC or directly contracted through you.

<!--Although we can provide services in both Tiers, for those services in the **1st Tier**, we can provide the most value specific to your project. -->

*Please lets us know if you would like us to include any of the following.*

  * As-built drawings for areas outside the scope.
	  * Sometimes an AHJ will request additional floor plans outside the scope area.
  * Civil engineering
       * Storm water management/calculations
       * Grading and erosion control plan/details
       * Storm water management/calculations
       * Retaining wall design
  * HVAC/Mechanical Design
  * Electrical Design
  * Lighting Design
  * Plumbing Design
  * Food Service or Commercial Kitchen Design
    <!-- * Kitchen equipment procurement -->
     <!-- * We will, however, suggest a schematic kitchen layout intended to be refined by a 3rd party kitchen vendor. -->
  * Furniture, Fixtures & Equipment (FF&E) services
  * Interior design
    * Picking out interior finishes
  * Casework/millwork elevations
  * [Large scale](http://openingdetail.com/gallery/) casework/millwork details
  * [Large scale](http://openingdetail.com/gallery/) interior details
  * Long form (book) construction specifications
  * Landscape design
  * Structural work associated with shoring during construction
  * Land Surveying
  * Low Voltage Design
  * Audio/Visual Design
  * Extensive energy modeling beyond prescriptive requirements
  * LEED Design
  * LEED Commissioning
  * Hyper-Realistic Renderings
  * Detailed Cost Estimation
  * Fire Alarm
  * Fire Protection (Sprinklers)
  * Security
  * Signage Design/Layout
  * Acoustical Engineering Services
  * Geotechnical Engineering
  * Environmental Studies and Reports
  * Materials testing or inspections
  * Information Technology
  * Legal Survey
  * Closeout Record Documents
  * Testing and Balancing of Installed Equipment
  * Environmental Studies
  * Commissioning Services
  * Moving Coordination
  * Post-occupancy Elevation/Studies
  * Maintenance and Operational Programming
  * Building Maintenance Manuals
  * Post-occupancy Evaluation
  * Extensive decorative finish studies
  * Art selection
  * Artwork Production Services 
  * Additional art commission services may be provided to client and quoted separately


<br>
<br>
<br>
<br>



## Proposed Fee:

<table cellspacing="0" border="0">
	<colgroup width="94"></colgroup>
	<colgroup width="236"></colgroup>
	<colgroup span="2" width="90"></colgroup>
	<tbody><tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="42" align="center" valign="middle"><b><font face="Century Gothic"><br></font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Phase</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic">Not-To-Exceed</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle"><b><font face="Century Gothic">Estimated Duration</font></b></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.1" sdnum="1033;0;0.00%"><font face="Century Gothic">10.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 1: <br>Pre-Design &amp; Programming</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="399.99996" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$400</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="0.49999995" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">0.5 wks</font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.15" sdnum="1033;0;0.00%"><font face="Century Gothic">15.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 2: <br>Schematic Design</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="599.99994" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$600</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="0.749999925" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">0.7 wks</font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.2" sdnum="1033;0;0.00%"><font face="Century Gothic">20.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 3: <br>Design Development</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="799.99992" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$800</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="0.9999999" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">1.0 wks</font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.3" sdnum="1033;0;0.00%"><font face="Century Gothic">30.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 4: <br>Construction Documents</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="1199.99988" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$1,200</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="1.49999985" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">1.5 wks</font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.05" sdnum="1033;0;0.00%"><font face="Century Gothic">5.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 5: <br>Bidding and Issuing for Permit</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="199.99998" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$200</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdval="0.249999975" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">0.2 wks</font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="34" align="center" valign="middle" sdval="0.2" sdnum="1033;0;0.00%"><font face="Century Gothic">20.00%</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle"><font face="Century Gothic">Phase 6: <br>Construction Administration</font></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" sdval="799.99992" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><font face="Century Gothic">$800</font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" sdnum="1033;0;#,##0.0&quot; wks&quot;"><font face="Century Gothic">n/a</font></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" height="62" align="center" valign="middle" bgcolor="#DDDDDD" sdval="1" sdnum="1033;0;0.00%"><b><font face="Century Gothic">100.00%</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="left" valign="middle" bgcolor="#DDDDDD"><b><font face="Century Gothic">All Phases</font></b></td>
		<td style="border-top: 1px solid #666666; border-bottom: 1px solid #666666; border-left: 1px solid #666666; border-right: 1px solid #666666" align="center" valign="middle" bgcolor="#DDDDDD" sdval="3999.9996" sdnum="1033;0;[$$-409]#,##0;-[$$-409]#,##0"><b><font face="Century Gothic">$4,000</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign="middle" bgcolor="#DDDDDD" sdval="3.9999996" sdnum="1033;0;#,##0.0&quot; wks &quot;"><b><font face="Century Gothic">4.0 wks </font></b></td>
	</tr>
</tbody></table>


<!-- > Please note, these are not **lump sum** fees, but are instead, **not-to-exceed** fees.  The **Hourly Rates**, called out below, will apply until this not-to-exceed fee is reached.  -->



## Hourly Rates

<!--
> Please Note:  Services performed in Phase 1 (Programming) will be discounted **25%** discount.
-->

<table cellspacing="0" border="0">
	<colgroup width="133"></colgroup>
	<colgroup width="85"></colgroup>
	<tbody><tr>
		<td height="34" align="center" valign="middle"><b><font face="Century Gothic">Discipline</font></b></td>
		<td align="center" valign="middle"><b><font face="Century Gothic">Rate</font></b></td>
	</tr>
	<tr>
		<td colspan="2" height="17" align="left" valign="middle"><b><font face="Century Gothic">Architecture</font></b></td>
		</tr>
	<tr>
		<td height="17" align="left" valign="middle"><font face="Century Gothic">Principal</font></td>
		<td align="center" valign="middle" sdval="120" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$120/HR</font></td>
	</tr>
	<tr>
		<td height="17" align="left" valign="middle"><font face="Century Gothic">Project Architect</font></td>
		<td align="center" valign="middle" sdval="100" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$100/HR</font></td>
	</tr>
	<tr>
		<td height="17" align="left" valign="middle"><font face="Century Gothic">Intern</font></td>
		<td align="center" valign="middle" sdval="75" sdnum="1033;0;&quot;$&quot;#,##0&quot;/HR&quot;"><font face="Century Gothic">$75/HR</font></td>
	</tr>
</tbody></table>


<!-- > ### *The Consultants fees, listed above, will include an **additional 15%** to cover in-house administration, handling, financing, and insurance costs.* -->

<!-- --- -->

<!--

## The Determination of the Not-to-Exceed Fee

For your reference and peace of mind, please review the following 3rd party document(s) as to what the standard practices are for establishing design fees in the construction/architecture industry.

* [Architectural Fees (part 1)](http://www.lifeofanarchitect.com/architectural-fees-part-1/) by Bob Borson
 * [Architectural Fees…part two](http://www.lifeofanarchitect.com/architectural-fees-part-two/) by Bob Borson
 * [Architectural Fees for Residential Projects](http://www.lifeofanarchitect.com/architectural-fees-for-residential-projects/) by Bob Borson
* [A Guide to Determining Appropriate Fees  for the Services of an Architect](https://network.aia.org/HigherLogic/System/DownloadDocumentFile.ashx?DocumentFileKey=f7ea7369-a39e-4310-837d-f0d0d0c06270) from  Royal Architectural Institute of Canada

As you will see relative to the suggested fees outlined in these documents, our fees are competitive in comparison.  We are confident that through our unique and open way of working and our strong band of collaborators and consultants, that that we will meet and exceed the industry's standard of care.

-->

---

## Reimbursable expenses include:

* Transportation in connection with the project for travel authorized by the client (transportation, lodging and meals)
  * $1.00 per mile for travel
  * Hourly billing rates while traveling
* Communication and shipping costs (long distance charges, courier, postage, dedicated web hosting, etc.)
* Reproduction costs for plans, sketches, drawings, graphic representations and other documents
* Renderings, models, prints of computer-generated drawings, mock-ups specifically    requested     by the client
* Certification and documentation costs for third party    certification    such    as LEED
* Fees, levies, duties or taxes for permits, licenses, or approvals from authorities having jurisdiction
* Additional insurance coverage or limits, including additional professional liability insurance requested by the client in a excess of that normally carried by the designer and the designer’s consultants
* Direct expenses from additional consultants not specifically outlined in this proposal

*Reimbursable expenses include an additional 15% to cover in-house administration,    handling,    and    financing.*

---

## Boilerplate

* We will deliver invoices on a monthly basis based on scope complete, with payment due within 30 days of receipt.  Invoices overdue past (60) days will be interpreted as an order to stop work on the project and a basis for termination of contract.
* We are not responsible to select a general contractor or guarantee workmanship performed by contractor or other parties. We can, however, make suggestions or recommendations for craftsman or contractors for Client to consider
* We not responsible for the exploration, presence, handling, and/or adverse exposure of any hazardous materials, in any from. Including, but not limited to asbestos products, polychlorinated biphenyl (pcb) or other toxic substances.
* Both Parties agree to communicate about any pertinent information and known changes to the project scope in a timely manner by writing.
* In the event of premature project termination, we will bill for percentage of work completed to date, based on the enclosed hourly rate.
* If you decide to forgo the project, fees, from all phases complete, are due regardless of the following scenerios:
  * Construction costs are more than you expected, for whatever reason.
  * Code requirements, required by the AHJ, at any phase of the project make construction costs too costly to continue with the project. 
  
  <!--* If no site survey is supplied, this proposal does include services to change the proposed design if the base assumptions such as property line locations and setbacks, as provided by our clients, is incorrect at later stages of the design.-->
* This proposal is valid for 90 days.

---

We sincerely appreciate the opportunity to submit this proposal and look forward to the potential of a fruitful collaboration in the future.

If I <!--we--> have included a service, within this proposal, that is not necessary and/or one that you would like to include, please let me <!--us--> know. 

<!--If the terms of this proposal are acceptable please sign in the space offered below and remit a $2000 retainer.
-->

Finally, please don't hesitate to contact me should you have any questions or need clarification about the proposal--would be more than happy to sit down and have a more nuanced discussion.

<br>

Kind Regards,

<img src="https://dl.dropboxusercontent.com/s/wwm6lvp04kvj6cn/signature.png?dl=1" width="200px"/>

Ryan Schultz
ryan.schultz@openingdesign.com
773.425.6456
17 S. Fairchild, FL 7
Madison, Wisconsin 53703




### Authorized by:



<br>


.................................................................................................................

* Name:

.................................................................................................................



* Title:
  
.................................................................................................................



<!-- * Company:

................................................................................................................. -->


* Date:

.................................................................................................................


* Signature:

.................................................................................................................






###  License

Per usual, unless otherwise noted, all content associated with [OpeningDesign](http://openingdesign.com) projects is licensed under an open source, 'copyleft' license: [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/) ![Attribution-ShareAlike 4.0 International](http://i.creativecommons.org/l/by-sa/3.0/88x31.png).  Attribution to be given to the entire team associated with the project.

